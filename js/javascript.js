//id funktion
let $ = (foo) => {
    return document.getElementById(foo);
}
//class funktion
let $2 = (bar) => {
    return document.getElementsByClassName(bar);
}
//global tom objekt så fler funktioner kan tilgå den

//validere om tidinputtet indenholder nedstående tegn
let tidSet = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;

let bookingInfo = {
    name: "Thomas",
    amount: "4",
    date: "",
    time: ""
}
let bookingDate = [];

//viser booking infomation
let showDate = () => {
$("confirmationText").innerHTML ="Der er blevet reseveret et bord til" + $("nameText").value;
     }

//boolean local storage enabled og catches exceptions
const isLocalStorageEnabled = () => {
    let foo = "bar";
    try {
        localStorage.setItem(foo, foo);
        localStorage.removeItem(foo);
        return true;
    } catch (e) {
        return false;
    }
}

//validerer om man har udfyldt navn, dato, dato i fremtiden og tidsfeltet. Hvis de ikker er udfyldt genneføres bestillingen ikke og der kommer en besked.
const validate = (e) => {
          message = []
          var today = new Date();
          var dateString = ($("date")).value;
          var myDate = new Date(dateString);

          if ($("nameText").value.length <= 1) {
            message.push("Udfyld et gyldigt navn");
            return false;
          }
          if ($("date").value.length !== 10) {
            message.push("Indsæt dato");
            return false;
          }
          if (myDate < today) {
            message.push("Du skal bestille senest dagen før din booking");
            return false;
          }
          if (tidSet.test($("time").value)) {               // returns true
            } else {
              message.push("Indsæt tidspunkt");
            return false;
          }
      return true;
          }

//sets values to correct object, pushes into array when isLocalStorageEnabled = true, and sets items in localStorage
//Så køre den validate funktionen igennem. Hvis noget ikke er udfyldt korrekt vil funktionen afbrydes. Så der ikke bliver gemt ugyldigt data i local storage.
let writeDate = () => {
    if (isLocalStorageEnabled) {
        $("confirmBtn").addEventListener("click", (e) => {

          if (validate(e) === true) {
            showDate();//måske trigger
            let getItem = localStorage.getItem('bookingDate');
            let bookingDate = getItem ? JSON.parse(getItem) : [];
            let bk = Object.assign({}, bookingInfo);
            bk.name = $("nameText").value;
            bk.amount = $("amountText").value;
            bk.date = $("date").value;
            bk.time = $("time").value;
            bookingDate.push(bk);
            let date = JSON.stringify(bookingDate);
            localStorage.setItem("bookingDate", date);

}
          if (validate(e) === false) {
            $("error").innerText = message.join("Bookingen er ikke udfyldt korrekt!")
          }
        });
      }
    }

window.onload = function() {
    writeDate();

};
